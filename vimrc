" get rid of Vi compatibility mode. SET FIRST!
" Compatible mode means compatibility to old vi. When you 
" :set compatible
" all the enhancements and improvements of Vi Improved are turned
" off.
set nocompatible

filetype off                  " Vundle - required

" Vundle - plugin manager
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.

"""------ UTILITY ------"""
" NERDTree
" File system explorer for the Vim editor
Plugin 'preservim/nerdtree'

" Tagbar
" Class viewer, creating a sidebar that displays the ctags-generated tags of the current file, ordered by their scope. This means that for example methods are displayed under the class they are defined in.
Plugin 'majutsushi/tagbar'

" FZF
" general-purpose command-line fuzzy finder
Plugin 'junegunn/fzf'

" Vim Airline
" Beautify status bar
Plugin 'vim-airline/vim-airline'

" AutoComplPop
Plugin 'AutoComplPop'

"""------ GENERIC PROGRAMMING SUPPORT ------"""
" Syntastic
" Syntax checking plugin. It runs files through external syntax checkers and displays any resulting errors
Plugin 'scrooloose/syntastic'


"""------ GIT SUPPORT ------"""
" Vim Fugitive
" Calls git like, :Gadd ($git add)
Plugin 'tpope/vim-fugitive'

" Vim Gitgutter
" Add icons to show a git diff in the sign column. It shows which lines have been added, modified, or removed
Plugin 'airblade/vim-gitgutter'

" List ends here. Plugins become visible to Vim after this call.
call vundle#end()

filetype plugin indent on

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"---- AutoComplPop settings ----"
" Spell check from dictionary
set complete+=kspell
" Use options (for more info, check :help completeopt)
set completeopt=menuone,longest,preview
" No msg is shown under the statusbar
set shortmess+=c

"---- NERDTree settings ----"
" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif


if has("autocmd")
    filetype plugin indent on " filetype detection[ON] plugin[ON] indent[ON]

    " In Makefiles DO NOT use spaces instead of tabs
    autocmd FileType make setlocal noexpandtab
    " In Ruby files, use 2 spaces instead of 4 for tabs
    autocmd FileType ruby setlocal sw=2 ts=2 sts=2

    " Enable omnicompletion (to use, hold Ctrl+X then Ctrl+O while in Insert mode.
    set ofu=syntaxcomplete#Complete
    filetype off
endif "has("autocmd")

set t_Co=256              " enable 256-color mode.
syntax enable             " enable syntax highlighting (previously syntax on).

" Better command-line completion
set wildmenu

" Prettify JSON files
autocmd BufRead,BufNewFile *.json set filetype=json
autocmd Syntax json sou ~/.vim/syntax/json.vim

" Prettify Vagrantfile
autocmd BufRead,BufNewFile Vagrantfile set filetype=ruby

" Prettify Markdown files
augroup markdown
    au!
    au BufNewFile,BufRead *.md,*.markdown setlocal filetype=markdown
augroup END

"Key binding
"from
"http://vim.wikia.com/wiki/Toggle_auto-indenting_for_code_paste
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" Autoclose opened brakets, parenthesis and braces
inoremap {      {}<Left>
inoremap {<CR>  {<CR>}<Esc>O
inoremap {{     {
inoremap {}     {}

inoremap [      []<Left>
inoremap [<CR>  [<CR>]<Esc>O
inoremap [[     [
inoremap []     []

inoremap (      ()<Left>
inoremap (<CR>  (<CR>)<Esc>O
inoremap ((     (
inoremap ()     ()

"Show command in the last line of the screen
set showcmd

"Line number"
:set number
"Make the number gutter 5 characters wide
set numberwidth=5

" Don't continue to highlight searched phrases.
set nohlsearch
" But do highlight as you type your search.
set incsearch

"Activate mouse"
:set mouse=a

" Always show info or cursor along bottom.
:set ruler

" Autoindent
:set autoindent
"Automatically insert one extra level of indentation
:set smartindent
"Tab spacing"
:set tabstop=4
"Indent/outdent by 4 columns
:set shiftwidth=4
"Always indent/outdent to the nearest tabstop
:set shiftround

"Transform tabs into spaces"
:set expandtab

" Use native colorscheme
colorscheme morning
" And customize
"Show cursor line and change its appearance"
:set cursorline
:hi CursorLine cterm=UNDERLINE ctermbg=NONE
" Highlight characters that go over 80 columns (by drawing a border on the 81st)
:set colorcolumn=81
:highlight ColorColumn ctermbg=darkgray ctermfg=white

"From Vim help (use command :help usr_01.txt)

"Show links between bars | |
:set conceallevel=0
:hi link HelpBar Normal
:hi link HelpStar Normal

"copied from $VIMRUNTIME/vimrc_Example.vim from HELP
:set backspace=indent,eol,start

"Set a backup file
:set backup
"With extensionnn .bakvim
":set backupext=.bakvim

" When opening a new :terminal in vim
set termwinsize=10x0   " Set terminal size
set splitbelow         " Always split below

"Switching to another buffer, opening a new tab instead splitting window
set switchbuf=usetab,newtab
nnoremap <F8> :sbnext<CR>
nnoremap <S-F8> :sbprevious<CR>

"from 
"github.com/VundleVim/Vundle.vim/issues/640
set rtp+=~/.vim/bundle/vundle.vim
