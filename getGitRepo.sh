#!/bin/bash

bundlePATH=./vim/bundle

echo "Must have some packages installed:\n\n
    * build-essential\n
    * cmake\n
    * python-dev\n
    * python3-dev\n\n"
echo "Essentially for YouCompleteMe (ycpm)"

sudo apt-get install build-essential cmake python-dev python3-dev 

echo "For YouCompleteMe: \n\n
    * cmake\n
    * clang-3.6\n"
echo "For YouCompleteMe plugin"

sudo apt-get install clang-3.6 cmake


git clone https://github.com/wincent/command-t.git $bundlePATH/command-t

git clone https://github.com/eparreno/vim-l9.git $bundlePATH/L9

git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git $bundlePATH/nerdtree-git-plugin

git clone https://github.com/Xuyuanp/nerdtree-git-plugin.git $bundlePATH/nerdtree-git-plugin

git clone https://github.com/rstacruz/sparkup.git $bundlePATH/sparkup

git clone http://github.com/tpope/vim-fugitive.git $bundlePATH/vim-fugitive

git clone https://github.com/airblade/vim-gitgutter.git $bundlePATH/vim-gitgutter

git clone https://github.com/Valloric/YouCompleteMe.git $bundlePATH/YouCompleteMe

git clone https://github.com/VundleVim/Vundle.vim.git $bundlePATH/Vundle.vim




git submodule add https://github.com/wincent/command-t.git $bundlePATH/command-t

git submodule add https://github.com/eparreno/vim-l9.git $bundlePATH/L9

git submodule add https://github.com/Xuyuanp/nerdtree-git-plugin.git $bundlePATH/nerdtree-git-plugin

git submodule add https://github.com/Xuyuanp/nerdtree-git-plugin.git $bundlePATH/nerdtree-git-plugin

git submodule add https://github.com/rstacruz/sparkup.git $bundlePATH/sparkup

git submodule add http://github.com/tpope/vim-fugitive.git $bundlePATH/vim-fugitive
vim -u NONE -c "helptags vim-fugitive/doc" -c q

git submodule add https://github.com/airblade/vim-gitgutter.git $bundlePATH/vim-gitgutter

echo "Check requirements at https://github.com/Valloric/YouCompleteMe/blob/master/README.md#installation"
cd $bundlePATH/YouCompleteMe
mkdir ycm_build && cd ycm_build
echo "Assuming you're using Ubuntu 14.04, downloading Libclang"
wget llvm.org/releases/3.8.0/clang+llvm-3.8.0-x86_64-linux-ntgnu-ubuntu-14.04.tar.xz
########################## check stop!!!!!!!!!!!!! ########################## 
git submodule add https://github.com/Valloric/YouCompleteMe.git $bundlePATH/YouCompleteMe

git submodule add https://github.com/VundleVim/Vundle.vim.git $bundlePATH/Vundle.vim

git submodule update --init --recursive 
